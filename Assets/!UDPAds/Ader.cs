﻿using UnityEngine;
using System.Collections;

public class Ader : MonoBehaviour {

	public float adTime = 30;

	private float timer;

	// Use this for initialization
	void Start () {

		timer = adTime;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (timer <= 0) {
			AdsController.Instance().TryToShowMixedBanner();
			timer = adTime;
		} else {

			timer -= Time.deltaTime;
		}
	}
}
