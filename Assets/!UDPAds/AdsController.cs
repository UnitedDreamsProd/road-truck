﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;

public class AdsController : MonoBehaviour {

	private static AdsController instance;

	public bool useUnityAds = true;

	public float unityAdsChance = 50;

	public int unityAdsGameID = 61740;
	
	public string unityAdsDefaultZone = "defaultZone";

	public string unityAdsRewardedZone = "rewardedVideoZone";

	public bool atTop = true;
	public string topBannerUnitIdAndroid;
	private BannerView topBanner;

	public string largeBannerUnitIdAndroid;
	private InterstitialAd largeBanner;

	public string mixedBannerUnitIdAndroid;
	private InterstitialAd mixedBanner;

	public bool useTopBanner = true;
	public bool useLargeBanners = true;
	public bool useMixedBanners = true;

	public delegate void OnVideoFinished();
	
	public OnVideoFinished onVideoFinished;

	private bool showing = false;
	//private float showingTimer = 60;

	public static AdsController Instance()
    {

        if (instance == null)
        {

            GameObject go = new GameObject("AdsController");
			instance = go.AddComponent<AdsController>();
        }

        return instance;
    }

    void Awake()
    {
		
        if (instance != null && instance != this) {
			DestroyImmediate(this.gameObject);
			return;
		}
        else instance = this;

		DontDestroyOnLoad (instance.gameObject);

		if(useTopBanner) ShowTopBanner ();
		
		if(useLargeBanners) InitLargeBanner ();

		if(useMixedBanners) InitMixedBanner ();

		Log ("Awake");
    }

//----------------------------------------------------------------------------------------------------------------------

	public void ShowTopBanner() {
	
		if (topBanner != null) {
			Log("Destroying top banner");
			topBanner.Destroy ();
		}

		#if UNITY_ANDROID
		string topBannerUnitId = topBannerUnitIdAndroid;
		#elif UNITY_IPHONE
		string topBannerUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
		#else
		string topBannerUnitId = "unexpected_platform";
		#endif
		
		
		// Create a 320x50 banner at the top of the screen.
		AdPosition position;
		if (atTop) position = AdPosition.Top;
		else position = AdPosition.Bottom;
		topBanner = new BannerView(topBannerUnitId, AdSize.Banner, position);
		// Create an empty ad request.
		//AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).Build();
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		topBanner.LoadAd(request);
		
		topBanner.AdLoaded += TopBannerLoaded;
		topBanner.AdOpened += TopBannerOpened;
		topBanner.AdFailedToLoad += TopBanneFailedToLoad;
		topBanner.AdClosed += TopBannerClosed;
	}

	public void TopBannerLoaded(object sender, EventArgs args) {

		Log ("Banner loaded");

		topBanner.Show ();
	}

	public void TopBannerOpened(object sender, EventArgs args) {

		//Log ("Banner opened");
	}

	public void TopBanneFailedToLoad(object sender, AdFailedToLoadEventArgs args) {

		ShowTopBanner ();
		//Log("Banner failed to load");
	}

	public void TopBannerClosed(object sender, EventArgs args) {

		ShowTopBanner ();
		//Log ("Banner closed");
	}
	
//-----------------------------------------------------------------------------------------------------------------

	public void InitLargeBanner() {
			
		#if UNITY_ANDROID
		string bannerUnitId = largeBannerUnitIdAndroid;
		#elif UNITY_IPHONE
		string bannerUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
		#else
		string bannerUnitId = "unexpected_platform";
		#endif

		largeBanner = new InterstitialAd (bannerUnitId);

		largeBanner.AdLoaded += LargeBannerLoaded;
		largeBanner.AdOpened += LargeBannerOpened;
		largeBanner.AdFailedToLoad += LargeBannerFailedToLoad;
		largeBanner.AdClosed += LargeBannerClosed;

		RequestLargeBanner ();
	}

	public void RequestLargeBanner() {
	
		if(largeBanner == null) return;

		Log("Requesting large banner");

		AdRequest request = new AdRequest.Builder ().Build ();
		largeBanner.LoadAd (request);
	}

	public void TryToShowLargeBanner() {

		if(largeBanner == null) return;

		if(showing) return;

		if (useUnityAds && UnityEngine.Random.Range(0.0f, 100.0f) < unityAdsChance && UnityAdsHelper.IsReady (unityAdsDefaultZone)) {
			showing = true;
			UnityAdsHelper.onFinished = BannerClosed;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd(unityAdsDefaultZone);
			return;
		}

		bool showed = false;
		if(largeBanner.IsLoaded()) {
			showing = true;
			largeBanner.Show ();
			showed = true;
		}

		if (useUnityAds && !showed && UnityAdsHelper.IsReady (unityAdsDefaultZone)) {
			showing = true;
			UnityAdsHelper.onFinished = BannerClosed;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd(unityAdsDefaultZone);
			return;
		}
	}

	public void LargeBannerLoaded(object sender, EventArgs args) {
		
		Log ("Large banner loaded");
	}
	
	public void LargeBannerOpened(object sender, EventArgs args) {

		//Log ("Large banner opened");
	}
	
	public void LargeBannerFailedToLoad(object sender, AdFailedToLoadEventArgs args) {

		RequestLargeBanner ();

		//Log("Large banner failed to load");
	}

	public void LargeBannerClosed(object sender, EventArgs args) {

		showing = false;

		RequestLargeBanner ();

		//Log("Large banner closed");
	}

//--------------------------------------------------------------------------------------------------------------------

	public void TryToShowVideoBanner() {

		if(showing) return;
		
		if (useUnityAds && UnityAdsHelper.IsReady (unityAdsRewardedZone)) {
			showing = true;
			UnityAdsHelper.onFinished = VideoFinished;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd(unityAdsRewardedZone);
			return;
		}
	}

	public void VideoFinished() {
	
		Log ("Video finished");

		showing = false;

		if(onVideoFinished != null) onVideoFinished();
	}

	public void BannerClosed() {
	
		showing = false;
	}
//-----------------------------------------------------------------------------------------------------------------

	public void InitMixedBanner() {
		
		#if UNITY_ANDROID
		string bannerUnitId = mixedBannerUnitIdAndroid;
		#elif UNITY_IPHONE
		string bannerUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
		#else
		string bannerUnitId = "unexpected_platform";
		#endif
		
		mixedBanner = new InterstitialAd (bannerUnitId);
		
		mixedBanner.AdLoaded += MixedBannerLoaded;
		mixedBanner.AdOpened += MixedBannerOpened;
		mixedBanner.AdFailedToLoad += MixedBannerFailedToLoad;
		mixedBanner.AdClosed += MixedBannerClosed;
		
		RequestMixedBanner ();
	}
	
	public void RequestMixedBanner() {

		if(mixedBanner == null) return;

		Log("Requesting mixed banner");
		
		AdRequest request = new AdRequest.Builder ().Build ();
		mixedBanner.LoadAd (request);
	}
	
	public void TryToShowMixedBanner() {

		if(mixedBanner == null) return;

		if(showing) return;
		
		if (useUnityAds && UnityEngine.Random.Range(0.0f, 100.0f) < unityAdsChance && UnityAdsHelper.IsReady (unityAdsDefaultZone)) {
			showing = true;
			UnityAdsHelper.onFinished = BannerClosed;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd(unityAdsDefaultZone);
			return;
		}
		
		bool showed = false;
		if(mixedBanner.IsLoaded()) {
			showing = true;
			mixedBanner.Show ();
			showed = true;
		}
		
		if (useUnityAds && !showed && UnityAdsHelper.IsReady (unityAdsDefaultZone)) {
			showing = true;
			UnityAdsHelper.onFinished = BannerClosed;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd(unityAdsDefaultZone);
			return;
		}
	}
	
	public void MixedBannerLoaded(object sender, EventArgs args) {
		
		Log ("Mixed banner loaded");
	}
	
	public void MixedBannerOpened(object sender, EventArgs args) {
		
		//Log ("Mixed banner opened");
	}
	
	public void MixedBannerFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
		
		RequestMixedBanner ();
		
		//Log("Mixed banner failed to load");
	}
	
	public void MixedBannerClosed(object sender, EventArgs args) {
		
		showing = false;
		
		RequestMixedBanner ();
		
		//Log("Mixed banner closed");
	}

//----------------------------------------------------------------------------------------------------------------------

	// Use this for initialization
	void Start () {
	    
		Log ("Start");

		UnityAdsSettings settings = (UnityAdsSettings)Resources.Load("UnityAdsSettings");
		
		if (settings == null)
		{
			Debug.LogError("Failed to initialize Unity Ads. Settings file not found.");
			return;
		}

		settings.androidGameId = unityAdsGameID.ToString ();
		settings.iosGameId = unityAdsGameID.ToString ();

		UnityAdsHelper.Initialize();
	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.Escape)) {
		
			//ShowLargeBanner();
		}

		/*if (showing) {
		
			showingTimer -= Time.deltaTime;

			if(showingTimer <= 0) {
			
				showingTimer = 60;
				showing = false;
			}
		}//*/
	}

	void OnDestroy() {

		Log ("Destroying banner");

		if(topBanner != null) topBanner.Destroy();

		if(largeBanner != null) largeBanner.Destroy();

		if(mixedBanner != null) mixedBanner.Destroy();
	}

	void OnLevelWasLoaded(int level) {

		TryToShowLargeBanner();

		Log ("Level loaded " + level);
	}

	public UnityEngine.UI.Text console;

	public void Log(string str) {

		if (console != null)
			console.text += " " + str + "\n";

		Debug.Log (str);
	}

	/*private bool quiting = false;
	private bool coroutineRunning = false;
	void OnApplicationQuit() {

		if (!quiting) {
			Application.CancelQuit ();
			if(!coroutineRunning) StartCoroutine (DelayedQuit ());
		}
	}

	IEnumerator DelayedQuit() {

		coroutineRunning = true;
		ShowMixedBanner ();
		yield return new WaitForSeconds(1);
		quiting = true;
	}//*/
}
