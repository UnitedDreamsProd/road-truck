﻿using UnityEngine;
using System.Collections;

public class OpenNextLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		AdsController.Instance().onVideoFinished = Open;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OpenLevel() {
	
		AdsController.Instance ().TryToShowVideoBanner ();
	}

	public void Open() {
	
		if (PlayerPrefs.GetInt("lvl2") != 1) {

			PlayerPrefs.SetInt("lvl2", 1);
			Application.LoadLevel("Lvl");
			return;
		}

		if (PlayerPrefs.GetInt("lvl3") != 1) {
			
			PlayerPrefs.SetInt("lvl3", 1);
			Application.LoadLevel("Lvl");
			return;
		}
	}
}
