﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Reflection;

public class CarSelecter : MonoBehaviour {

    public GameObject[] cars;
    public GameObject buttonGo;
    public GameObject buttonBuy;
    public Text textBalance;
    public Text textCost;

    void Start()
    {
        Account.SelectedCar = 1;
        RenderCar();
        RenderButtons();
        RenderTexts();
    }

    public void SelectLeft()
    {
        if (Account.SelectedCar > 1)
        {
            Account.SelectedCar--;
            RenderCar();
            RenderButtons();
            RenderTexts();
        }
    }

    public void SelectRight()
    {
        if (Account.SelectedCar < 5)
        {
            Account.SelectedCar++;
            RenderCar();
            RenderButtons();
            RenderTexts();
        }
    }

    public void Reklama()
    {
        Debug.Log("Tyta");
        AdsController.Instance().onVideoFinished = ReklamaShowed;
        AdsController.Instance().TryToShowVideoBanner();
    }
    private void ReklamaShowed()
    {
        Account.Balance += 100;
        Account.Balance = (int)Functions.Self.setValueToPlayerPrefs("Balance", Account.Balance);
        textBalance.text = "Your balance $" + Account.Balance;
    }

    public void Complete()
    {
        Functions.Self.loadMapSelectScene();
    }

    public void RenderCar()
    {
        foreach (GameObject car in cars)
        {
            car.SetActive(false);
        }
        cars[Account.SelectedCar - 1].SetActive(true);
    }

    public void RenderButtons()
    {
        Type carClass = Type.GetType("Car" + Account.SelectedCar);
        MethodInfo method = carClass.GetMethod("getBought");

        var bought = (int)method.Invoke(null, null);

        if (bought > 0)
        {
            buttonGo.SetActive(true);
            buttonBuy.SetActive(false);
        }
        else
        {
            buttonGo.SetActive(false);
            buttonBuy.SetActive(true);
        }
    }

    public void RenderTexts()
    {
        textBalance.text = "Your balance $" + Account.Balance;

        try
        {
            Type carClass = Type.GetType("Car" + Account.SelectedCar);
            MethodInfo method = carClass.GetMethod("getCost");
            textCost.text = "buy $ " + method.Invoke(null, null);
        }
        catch (Exception) { }
    }

    public void BuyCar()
    {
        try
        {
            Type carClass = Type.GetType("Car" + Account.SelectedCar);
            MethodInfo method = carClass.GetMethod("Buy");
            method.Invoke(null, null);
        }
        catch (Exception) { }

        RenderButtons();
        RenderTexts();

    }

}
