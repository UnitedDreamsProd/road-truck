﻿using UnityEngine;
using System.Collections;

public class Initialization : MonoBehaviour {
    
	void Start () {
        carInitialization();
    }

    private void carInitialization()
    {
        //PlayerPrefs.DeleteAll();
        //PlayerPrefs.Save();
        
        Account.Init();
        Car1.Init();
        Car2.Init();
        Car3.Init();
        Car4.Init();
        Car5.Init();
    }

}
