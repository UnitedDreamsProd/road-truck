﻿using UnityEngine;
using System.Collections;
using System;

public class Car2
{
    static public int Cost { get; set; }
    static public int Bought { get; set; }

    static public void Init()
    {
        Cost = 250;
        LoadBoughtFromPlayerPrefs();
    }

    static public int getCost()
    {
        return Cost;
    }

    static public int getBought()
    {
        return Bought;
    }

    static public void LoadBoughtFromPlayerPrefs()
    {
        try
        {
            Bought = (int)Functions.Self.loadValueFromPlayerPrefs("Car2_Bought", Bought.GetType());
        }
        catch (PlayerPrefsException)
        {
            Bought = (int)Functions.Self.setValueToPlayerPrefs("Car2_Bought", 0);
        }
    }

    static public void SetBoughtToPlayerPrefs()
    {
        Bought = (int)Functions.Self.setValueToPlayerPrefs("Car2_Bought", Bought);
    }

    static public void Buy()
    {
        if (Account.Balance >= Cost)
        {
            Account.Balance -= Cost;
            Account.setBalanceToPlayerPrefs();
            Debug.Log("TYT " + Account.Balance);
            Bought = 1;
            SetBoughtToPlayerPrefs();
        }
    }

}
