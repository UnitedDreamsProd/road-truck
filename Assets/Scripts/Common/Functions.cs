﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class Functions : MonoBehaviour {

    static public Functions Self { set; get; }

    void Start()
    {
        Self = new Functions();
    }

    /* PlayerPrefs functions */
    public object setValueToPlayerPrefs(string key, object value)
    {
        if (value.GetType() == typeof(int))
        {
            PlayerPrefs.SetInt(key, (int)value);
            PlayerPrefs.Save();
            //Debug.Log(key + " setted: " + value);
            return value;
        }
        if (value.GetType() == typeof(float))
        {
            PlayerPrefs.SetFloat(key, (float)value);
            PlayerPrefs.Save();
            //Debug.Log(key + " setted: " + value);
            return value;
        }
        if (value.GetType() == typeof(string))
        {
            PlayerPrefs.SetString(key, (string)value);
            PlayerPrefs.Save();
            //Debug.Log(key + " setted: " + value);
            return value;
        }

        throw new PlayerPrefsException("Forbidden type \"" + value.GetType());
    }
    public object loadValueFromPlayerPrefs(string key, Type type)
    {
        object value = 0;

        if (PlayerPrefs.HasKey(key))
        {
            if (type == typeof(int))
            {
                value = PlayerPrefs.GetInt(key);
            }
            if (type == typeof(float))
            {
                value = PlayerPrefs.GetString(key);
            }
            if (type == typeof(string))
            {
                value = PlayerPrefs.GetFloat(key);
            }

            //Debug.Log(key + " loaded: " + value);
            return value;
        }

        throw new PlayerPrefsException("The Key \"" + key + "\" is not exist");
    }

    /* Scene loaders */
    public void loadGreetingScene()
    {
        Application.LoadLevel("greeting_scene");
    }
    public void loadGarageScene()
    {
        Application.LoadLevel("garage_scene");
    }
    public void loadMapSelectScene()
    {
        Application.LoadLevel("map_select_scene");
    }
    public void loadMap1Scene()
    {
        Application.LoadLevel("map_1_scene");
    }
    public void loadMap1ScenePreloader()
    {
        Application.LoadLevel("map_1_scene_preloader");
    }
    public void loadMap2Scene()
    {
        Application.LoadLevel("map_2_scene");
    }
    public void loadMap2ScenePreloader()
    {
        Application.LoadLevel("map_2_scene_preloader");
    }
    public void loadMap3Scene()
    {
        Application.LoadLevel("map_3_scene");
    }
    public void loadMap3ScenePreloader()
    {
        Application.LoadLevel("map_3_scene_preloader");
    }
    public void loadLoseScene()
    {
        Application.LoadLevel("lose_scene");
    }
    public void loadWinScene()
    {
        Application.LoadLevel("win_scene");
    }

}
