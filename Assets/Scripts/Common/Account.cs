﻿using UnityEngine;
using System.Collections;

public class Account : MonoBehaviour {
    
    static public int Balance { get; set; }
    static public short SelectedCar { get; set; }

    static public void Init() {
        GetBalanceFromPlayerPrefs();
	}

    static public void GetBalanceFromPlayerPrefs()
    {
        //Balance = (int)Functions.Self.setValueToPlayerPrefs("Balance", 4000);
        try
        {
            Balance = (int)Functions.Self.loadValueFromPlayerPrefs("Balance", Balance.GetType());
        }
        catch (PlayerPrefsException)
        {
            Balance = (int)Functions.Self.setValueToPlayerPrefs("Balance", 150);
        }
        Debug.Log("TYT " + Balance);
    }

    static public void setBalanceToPlayerPrefs()
    {
        Balance = (int)Functions.Self.setValueToPlayerPrefs("Balance", Balance);
    }
}
