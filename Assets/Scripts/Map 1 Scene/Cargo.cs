﻿using UnityEngine;
using System.Collections;

public class Cargo : MonoBehaviour {

    public GameObject canvasLose;
    public GameObject buttonPause;
    public GameObject buttonFinish;

    #region Events
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("Terrain"))
        {
            //Time.timeScale = 0;
            buttonPause.SetActive(false);
            buttonFinish.SetActive(false);
            canvasLose.SetActive(true);
        }
    }
    #endregion

}
