﻿using UnityEngine;
using System.Collections;
using System;

public class RenderCar : MonoBehaviour {

    public GameObject[] cars;
    public GameObject[] cameras;

    void Start () {
        _RenderCar();
	}

    private void _RenderCar()
    {
        try {
            cars[Account.SelectedCar - 1].SetActive(true);
            cameras[Account.SelectedCar - 1].SetActive(true);
        } catch (Exception)
        {
            Account.SelectedCar = 1;
            cars[Account.SelectedCar - 1].SetActive(true);
            cameras[Account.SelectedCar - 1].SetActive(true);
        }
    }

}
