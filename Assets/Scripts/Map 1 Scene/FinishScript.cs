﻿using UnityEngine;
using System.Collections;

public class FinishScript : MonoBehaviour {

    public GameObject canvasWin;
    public GameObject buttonPause;

    void OnTriggerEnter(Collider other)
    {
        Account.Balance += 100;
        Account.setBalanceToPlayerPrefs();
        Time.timeScale = 0;
        buttonPause.SetActive(false);
        canvasWin.SetActive(true);
    }

    /*
    private void OnCollisionEnter(Collision collision)
    {
        Account.Balance += 100;
        Account.setBalanceToPlayerPrefs();
        Functions.Self.loadWinScene();
    }*/

}
