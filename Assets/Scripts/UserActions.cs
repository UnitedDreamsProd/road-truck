﻿using UnityEngine;

public class UserActions : MonoBehaviour
{

    public GameObject buttonPlay;
    public GameObject buttonPause;

    public GameObject canvasPause;

    public void Pause()
    {
        Time.timeScale = 0;
        buttonPlay.SetActive(true);
        buttonPause.SetActive(false);
        canvasPause.SetActive(true);
    }

    public void Play()
    {
        Time.timeScale = 1;
        buttonPlay.SetActive(false);
        buttonPause.SetActive(true);
        canvasPause.SetActive(false);
    }

    public void ToGarage()
    {
        Application.LoadLevel("garage_scene");
        Time.timeScale = 1;
    }

    public void Restart()
    {
        Application.LoadLevel(Application.loadedLevelName);
        Time.timeScale = 1;
    }

}
