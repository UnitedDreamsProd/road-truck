﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoaderMap2 : MonoBehaviour {

    public Image progressBar;

    IEnumerator Start()
    {
        AsyncOperation asynchLoadLevel = Application.LoadLevelAsync("map_2_scene");

        while (!asynchLoadLevel.isDone)
        {
            progressBar.fillAmount = asynchLoadLevel.progress;
            yield return null;
        }

        yield return asynchLoadLevel;
    }

}
