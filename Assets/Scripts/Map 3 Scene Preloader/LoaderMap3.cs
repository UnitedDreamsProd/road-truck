﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoaderMap3 : MonoBehaviour
{

    public Image progressBar;

    IEnumerator Start()
    {
        AsyncOperation asynchLoadLevel = Application.LoadLevelAsync("map_3_scene");

        while (!asynchLoadLevel.isDone)
        {
            progressBar.fillAmount = asynchLoadLevel.progress;
            yield return null;
        }

        yield return asynchLoadLevel;
    }

}
