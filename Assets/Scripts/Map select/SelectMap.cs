﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SelectMap : MonoBehaviour
{

    public GameObject canvas;
    public Image progressBar;
    public AsyncOperation asyncOperator;
    
    void Update()
    {
        try
        {
            progressBar.fillAmount = asyncOperator.progress;
        }
        catch (Exception) { }
    }

    public void LoadScene(string sceneName)
    {
        canvas.SetActive(true);
        asyncOperator = Application.LoadLevelAsync(sceneName);
        asyncOperator.allowSceneActivation = true;
    }

    public void loadMap1()
    {
        LoadScene("map_1_scene");
    }

    public void loadMap2()
    {
        LoadScene("map_2_scene");
    }

    public void loadMap3()
    {
        LoadScene("map_3_scene");
    }

}
